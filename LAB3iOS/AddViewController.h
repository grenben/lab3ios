//
//  AddViewController.h
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddViewController : UIViewController

@property () NSMutableArray *items;

@end
