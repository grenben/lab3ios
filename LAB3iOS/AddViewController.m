//
//  AddViewController.m
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "AddViewController.h"
#import "Task.h"
#import "TdolTableViewController.h"

@interface AddViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;

@property (weak, nonatomic) IBOutlet UITextField *taskNameTextField;

@property (weak, nonatomic) IBOutlet UITextField *taskDescriptionTextField;

@property (strong, nonatomic) Task *taskToAdd;

@end

@implementation AddViewController

- (IBAction)addItemDescriptionButtonPressed:(id)sender {

    //Task *taskToAdd = [[Task alloc] init];
    _taskToAdd.taskName = self.taskNameTextField.text;
    _taskToAdd.taskDescription = self.taskDescriptionTextField.text;
    [self.items addObject:_taskToAdd];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)importantTaskSwitch:(id)sender {
    if ([self.mySwitch isOn]) {
        _taskToAdd.importantTaskBool = YES;
        NSLog(@"Switch is on");
        [self.mySwitch setOn:NO animated:YES];
    } else {
        _taskToAdd.importantTaskBool = NO;
        [self.mySwitch setOn:YES animated:YES];
        NSLog(@"Switch is off");
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    _taskToAdd = [[Task alloc] init];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
