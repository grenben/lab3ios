//
//  DetailViewController.m
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "DetailViewController.h"
#import "TdolTableViewController.h"
#import "Task.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *taskDescription;

@property (weak, nonatomic) IBOutlet UILabel *taskName;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.taskDescription.text = self.taskDescriptionInfo;
    self.taskName.text = self.name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
