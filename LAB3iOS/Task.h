//
//  Task.h
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject <NSCoding>

@property (nonatomic)NSString *taskName;
@property (nonatomic)NSString *taskDescription;
@property (nonatomic)BOOL importantTaskBool;

-(instancetype)initWithName: (NSString *)taskName andDescription: (NSString *)taskDescription andImportantTaskBool:(BOOL)importantTaskBool;

-(id)initWithCoder:(NSCoder *)decoder;
-(void)encodeWithCoder:(NSCoder *)encoder;

+(Task *)createTaskWithName:(NSString *)name andTaskDescription:(NSString *)taskDescription andImportantTaskBool:(BOOL)importantTaskBool;


@end
