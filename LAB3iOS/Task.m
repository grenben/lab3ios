//
//  Task.m
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "Task.h"

@implementation Task

+(Task *)createTaskWithName:(NSString *)name andTaskDescription:(NSString *)taskDescription andImportantTaskBool:(BOOL)importantTaskBool {
    
    Task *newTask = [[Task alloc] init];
    
    [newTask setTaskName:name];
    [newTask setTaskDescription:taskDescription];
    [newTask setImportantTaskBool:importantTaskBool];
    
    return newTask;
}

-(instancetype)initWithName: (NSString *)taskName andDescription: (NSString *)taskDescription andImportantTaskBool:(BOOL)importantTaskBool {
    
    self = [super init];
    
    if (self) {
        _taskName = taskName;
        _taskDescription = taskDescription;
        _importantTaskBool = importantTaskBool;
    }
    
    return self;
}

-(void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.taskName forKey:@"taskName"];
    [encoder encodeObject:self.taskDescription forKey:@"taskDescription"];
    [encoder encodeBool:self.importantTaskBool forKey:@"importantTaskBool"];
}

-(id)initWithCoder:(NSCoder *)decoder {
    
    self = [super init];
    
    if (self) {
        self.taskName = [decoder decodeObjectForKey:@"taskName"];
        self.taskDescription = [decoder decodeObjectForKey:@"taskDescription"];
        self.importantTaskBool = [decoder decodeBoolForKey:@"importantTaskBool"];
    }
    
    return self;
}


@end
