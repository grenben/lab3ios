//
//  TdolTableViewController.h
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TdolTableViewController : UITableViewController

@property (nonatomic) NSMutableArray *items;



@end
