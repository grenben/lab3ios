//
//  TdolTableViewController.m
//  LAB3iOS
//
//  Created by Johan Grenlund on 2016-02-13.
//  Copyright © 2016 Johan Grenlund. All rights reserved.
//

#import "TdolTableViewController.h"
#import "AddViewController.h"
#import "Task.h"
#import "DetailViewController.h"

@interface TdolTableViewController ()


@end


@implementation TdolTableViewController

-(NSMutableArray*)items {
    
    if (!_items) {
        _items = [[NSMutableArray alloc] init];
    }
    
    return _items;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self items];
    
    NSLog(@"Items > %@", self.items);
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        self.title = @"Tasks to do";
        
        [self loadItems];
    }
    
    return self;
}

-(void)loadItems {
    NSString *filePath = [self pathForItems];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        self.items = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    } else {
        self.items = [NSMutableArray array];
    }
}

-(NSString *)pathForItems {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [paths lastObject];
    
    return [documents stringByAppendingPathComponent:@"items.plist"];
}

-(void)saveItems {
    NSString *filePath = [self pathForItems];
    [NSKeyedArchiver archiveRootObject:self.items toFile:filePath];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    [self saveItems];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell" forIndexPath:indexPath];
    
    Task *cellTask = self.items[indexPath.row];
    cell.textLabel.text = cellTask.taskName;
    
    if (cellTask.importantTaskBool) {
        cell.backgroundColor = [UIColor redColor];
    }
    
    [self saveItems];
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    
    
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.items removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self saveItems];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"ItemsDetail"]) {
        DetailViewController *destination = [segue destinationViewController];
        UITableViewCell *cell = sender;
        int row = (int)[self.tableView indexPathForCell:cell].row;
        destination.title = [self.items [row] taskName];
        destination.taskDescriptionInfo = [self.items [row] taskDescription];
        destination.name = [self.items [row] taskName];
        

    }
    else if ([segue.identifier isEqualToString:@"ItemAdd"]){
        AddViewController *addViewController = [segue destinationViewController];
        addViewController.items = self.items;
        
    }
    
    
}


@end
